#ifndef DET_TASK_H_
#define DET_TASK_H_

#include "../nlohmann/json.h"
#include "../utils/stopwatch.h"
#include "../utils/system_utils.h"
#include "./transition_relation.h"
#include "sdac_parser/create_add.h"
#include <cuddObj.hh>
#include <dddmp.h>
#include <map>
#include <memory>
#include <stdexcept>
#include <string>
#include <vector>

class BDDError : public std::exception {};
extern void exceptionCuddError(std::string message);

class DetTask {
public:
    void parse_task(std::string file, int round_dez, long timeout, size_t TRmax, size_t TRlen,
                    bool merge_transitions);
    cuddCPP::Cudd* get_cudd_mgr();
    cuddCPP::ADD get_var_ADD(std::string name);
    cuddCPP::ADD create_add(std::string term);
    void dump_dot(cuddCPP::ADD add, std::string filename);

    std::vector<TransitionRelation> get_trs() {
        return trs;
    }

    cuddCPP::ADD get_init_state() {
        return init_state;
    }

    cuddCPP::ADD get_goal_state() {
        return goal_state;
    }

    // Pass as copy because we later reset this task!
    std::vector<std::string> get_original_variable_names() {
        return original_variable_names;
    }

    // Pass as copy because we later reset this task!
    std::vector<int> get_variable_order() {
        return variable_order;
    }

    // Pass as copy because we later reset this task!
    std::vector<std::string> get_variable_names() {
        return variable_names;
    }

    // Pass as copy because we later reset this task!
    std::map<std::string, std::vector<int>>
    get_variables_names_to_bdd_indices() {
        return variables_names_to_bdd_indices;
    }

    int get_num_of_bdd_vars() {
        return variable_bdd_names.size();
    }

    void reset(bool kill_mgr = false);

private:
    std::unique_ptr<cuddCPP::Cudd> cudd_mgr;
    CreateADD add_creator;
    std::vector<std::string> original_variable_names;
    std::vector<std::string> variable_names;
    std::vector<std::string> variable_bdd_names;
    std::vector<TransitionRelation> trs;
    cuddCPP::ADD init_state;
    cuddCPP::ADD goal_state;
    int horizon;
    // Threshold to prune probabilities
    int round_dez;
    // Used for build_up_timeout
    long parse_timeout;
    std::vector<int> variable_order;
    std::map<std::string, int> variable_names_to_index;
    std::map<std::string, int> variable_names_to_domain_size;
    std::map<std::string, std::vector<int>> variables_names_to_bdd_indices;
    std::map<std::string, cuddCPP::ADD> var_ADD;
    std::map<std::string, std::vector<cuddCPP::BDD>> var_to_val_bdd;
    std::vector<cuddCPP::ADD> pre_vars;
    std::vector<cuddCPP::ADD> eff_vars;
    int num_BDD_vars; // #unprimed vars. #all vars is num_BDD_vars * 2
    cuddCPP::ADD valid_values;

    void setup_var_order(nlohmann::json& task_data);

    void parse_variables(nlohmann::json& task_data);
    void parse_init_state(nlohmann::json& task_data);
    void parse_goal_state(nlohmann::json& task_data);
    void parse_actions(nlohmann::json& task_data);

    void init_cudd_mgr(int num_all_bdd_vars);
    void setup_bdd_vars(int num_all_bdd_vars);
    void setup_binary_adds(int num_all_bdd_vars);
    void setup_var_val_bdds();
    void setup_valid_values();

    cuddCPP::BDD generate_bdd_var(const std::string var_name, int value);

    cuddCPP::BDD generate_bdd_var(const std::vector<int>& bddVars, int value);

    void mergeTrs(int max_time, int max_size);
};

#endif /* DET_TASK_H */

// Copyright 04.04.2018, University of Freiburg,
// Author: David Speck <speckd>.

/// TODO(speckd): Buddy should probably be replaced with CUDD or both set to
/// namespace
/// For now we used a namespace for the cudd c++ interface => cuddCPP

#ifndef DD_HEURISTIC_H_
#define DD_HEURISTIC_H_

#include "../nlohmann/json.h"
#include "closed_list.h"
#include <cuddObj.hh>
#include <memory>
#include <vector>

class DetTask;

class DDHeuristic {
private:
    ClosedList layers;
    std::map<std::string, std::vector<int>> vars_to_indices;
    int num_bdd_vars;
    std::vector<std::string> variable_names;
    std::vector<double> infinite_heuristic_values;
    double worst_infinite_heuristic_value;

    std::vector<int> sym_state;

public:
    void compute_dd_heuristic(DetTask& task, int horizon);
    double get_h(std::vector<int>& state, int step_remaining);
    size_t num_layers() {
        return layers.get_num_layer();
    }
};

#endif // DD_HEURISTIC_H_


#include "det_task.h"
#include "../utils/math_utils.h"
#include "cudd_util.h"
#include "sdac_parser/create_ast.h"
#include <algorithm>
#include <algorithm>
#include <cmath>
#include <fstream>
#include <iostream>
#include <memory>
#include <stdio.h>

using namespace cuddCPP;

void exceptionCuddError(std::string message) {
    std::cout << message << std::endl;
    throw BDDError();
}

void DetTask::parse_task(std::string file, int round_dez, long timeout, size_t TRmax, size_t TRlen,
                         bool merge_transitions) {
    this->round_dez = round_dez;
    add_creator = CreateADD(round_dez);
    parse_timeout = timeout;
    nlohmann::json task_data;
    std::ifstream i_stream(file);
    i_stream >> task_data;
    //std::cout << task_data.dump(4) << std::endl;

    // Check if TRs are too large => directly stop
    auto init_data = task_data["actions"];
    nlohmann::json::iterator it;
    size_t tr_length = 0;
    size_t tr_max = 0;
    for (it = init_data.begin(); it != init_data.end(); ++it) {
        std::string tr = it.value()["Tr"];
        tr_max = std::max(tr.length(), tr_max);
        tr_length += tr.length();
    }
    if (tr_max > TRmax || tr_length > TRlen) {
        std::cout << "TR-Max: " << tr_max << std::endl;
        std::cout << "TR-Len: " << tr_length << std::endl;
        exceptionCuddError("TR formulas too large => skip DD-Heuristic.");
    }

    setup_var_order(task_data);

    parse_variables(task_data);
    parse_init_state(task_data);
    parse_goal_state(task_data);
    parse_actions(task_data);

    if (merge_transitions) {
        mergeTrs(60, 100000);
    }

    // dump_dot(init_state, "init_state.dot");
    // dump_dot(goal_state, "goal_state.dot");
    // for (auto& tr : trs) {
    //     dump_dot(tr.get_action_add(), tr.get_name() + "action.dot");
    //     dump_dot(tr.get_cost_add(), tr.get_name() + "cost.dot");
    //     dump_dot(tr.get_tr_add(), tr.get_name() + "tr.dot");
    // }
    // dump_dot(valid_values, "valid.dot");
}

// TODO(speckd): Rename all name vectors to reasonable names!!!
void DetTask::setup_var_order(nlohmann::json& task_data) {
    auto var_entry = task_data["variables"];

    // Collect all names
    nlohmann::json::iterator it;
    for (it = var_entry.begin(); it != var_entry.end(); ++it) {
        original_variable_names.push_back(it.key());
    }

    // Sort vars in increasing order var1,...,var9,var10
    std::sort(original_variable_names.begin(), original_variable_names.end(),
              [](std::string var1, std::string var2) -> bool {
                  if (var1.length() < var2.length()) {
                      return true;
                  }
                  if (var1.length() > var2.length()) {
                      return false;
                  }
                  return var1 < var2;
              });
    std::cout << "Original ordering: ";
    for (const auto& var : original_variable_names) {
        std::cout << var << " ";
    }
    std::cout << std::endl;

    // Collect all variables for the cost term (s.t we get an vector with all
    // vars)
    // Note: oposite order because we reverse later the ordering
    std::string full_sdac = "";
    for (auto& var : original_variable_names) {
        full_sdac = " + " + var + full_sdac;
    }
    full_sdac = "(0" + full_sdac + " + ";

    // add sdac cost
    // int rnd_n_actions =
    //    std::min(static_cast<size_t>(20), task_data["actions"].size() - 1);

    for (auto& ac : task_data["actions"]) {
        // auto it = MathUtils::rnd->select(task_data["actions"].begin(),
        //                                 task_data["actions"].end());
        std::string cur_cost = ac["Tc"];
        // We need to restirct the size to save time and to avoid a parser
        // problem
        if (cur_cost.size() + full_sdac.size() > 10000) {
            break;
        }
        full_sdac += cur_cost + " + ";
    }
    full_sdac += "0)";

    // std::cout << full_sdac << std::endl;
    // std::cout << full_sdac.size() << std::endl;

    // Compute new ordering
    std::cout << "Build ast..." << std::flush;
    CreateAST ast;
    ASTNode root_node = ast.create_ast(full_sdac);
    std::cout << "..done!" << std::endl;
    std::cout << "Compute fan-in..." << std::flush;
    std::vector<std::string> var_order =
        ast.get_fan_in_ordering(root_node, original_variable_names.size());
    std::reverse(var_order.begin(), var_order.end());
    std::cout << "done!" << std::endl;

    std::cout << "Fan-in ordering: ";
    for (const auto& var : var_order) {
        std::cout << var << " ";
    }
    std::cout << std::endl;

    // Set var names
    for (auto var : var_order) {
        variable_names.push_back(var);
        variable_names.push_back(var + "_primed");
    }

    for (auto& var : original_variable_names) {
        int pos =
            find(var_order.begin(), var_order.end(), var) - var_order.begin();
        variable_order.push_back(pos);
        std::cout << "[" << var << " : " << pos << "] ";
    }
    std::cout << std::endl;
}

void DetTask::parse_variables(nlohmann::json& task_data) {
    num_BDD_vars = 0;
    int cur_num_bdd_vars = 0;

    auto var_entry = task_data["variables"];
    for (auto& var_name : variable_names) {
        // Skip primed vars
        if (var_name.find("primed") != std::string::npos) {
            continue;
        }
        std::string var_name_primed = var_name + "_primed";
        int domain_size = var_entry[var_name]["domain"];
        variable_names_to_domain_size[var_name] = domain_size;
        variable_names_to_domain_size[var_name_primed] = domain_size;

        // Build up bdd vars
        variables_names_to_bdd_indices[var_name] = std::vector<int>();
        variables_names_to_bdd_indices[var_name_primed] = std::vector<int>();
        int var_len = ceil(log2(domain_size));
        num_BDD_vars += var_len;
        for (int j = 0; j < var_len; j++) {
            variable_bdd_names.push_back(var_name + "_2^" + std::to_string(j));
            variable_bdd_names.push_back(var_name_primed + "_2^" +
                                         std::to_string(j));
            variables_names_to_bdd_indices[var_name].push_back(
                cur_num_bdd_vars);
            variables_names_to_bdd_indices[var_name_primed].push_back(
                cur_num_bdd_vars + 1);
            cur_num_bdd_vars += 2;
        }
    }
    std::cout << "Num variables: " << var_entry.size() << " => " << num_BDD_vars
              << " [ incl. primed: " << cur_num_bdd_vars << " ]" << std::endl;

    init_cudd_mgr(cur_num_bdd_vars);
    setup_binary_adds(cur_num_bdd_vars);
    setup_var_val_bdds();
    setup_valid_values();
}

void DetTask::init_cudd_mgr(int num_all_bdd_vars) {
    /*long cudd_init_cache_size = 16000000L;
    long cudd_init_available_memory = 0L;
    long cudd_init_nodes = 16000000L;
    std::cout << "Initialize Symbolic Manager(" << num_all_bdd_vars << ", "
              << cudd_init_nodes / num_all_bdd_vars << ", "
              << cudd_init_cache_size << ", " << cudd_init_available_memory
              << ")" << std::endl;
    cudd_mgr = std::make_unique<Cudd>(
        num_all_bdd_vars, 0, cudd_init_nodes / num_all_bdd_vars,
        cudd_init_cache_size, cudd_init_available_memory);*/
    cudd_mgr = std::make_unique<Cudd>(num_all_bdd_vars);
    size_t max_bytes = 2147483648;     //~ 2 GB ,  26214400 ~ 25mb
    long time_limit = parse_timeout;   // in ms
    cudd_mgr->SetMaxMemory(max_bytes); //
    cudd_mgr->setHandler(exceptionCuddError);
    cudd_mgr->setTimeoutHandler(exceptionCuddError);
    cudd_mgr->setNodesExceededHandler(exceptionCuddError);
    Cudd_SetTimeLimit(cudd_mgr->getManager(), time_limit);
}

void DetTask::setup_binary_adds(int num_all_bdd_vars) {
    for (int i = 0; i < num_all_bdd_vars; i++) {
        if (i % 2 == 0) {
            pre_vars.push_back(cudd_mgr->addVar(i));
        } else {
            eff_vars.push_back(cudd_mgr->addVar(i));
        }
    }
}

void DetTask::setup_var_val_bdds() {
    for (auto& var : variable_names) {
        var_to_val_bdd[var] = std::vector<BDD>();
        for (int val = 0; val < variable_names_to_domain_size[var]; val++) {
            var_to_val_bdd[var].push_back(generate_bdd_var(var, val));
        }
    }
}

void DetTask::setup_valid_values() {
    valid_values = cudd_mgr->addZero();
    for (auto& var : variable_names) {
        valid_values += remove_cost(get_cudd_mgr(), get_var_ADD(var));
    }

    valid_values = valid_values.Equals(cudd_mgr->plusInfinity()) *
                   cudd_mgr->plusInfinity();
}

void DetTask::parse_init_state(nlohmann::json& task_data) {
    auto init_data = task_data["initial_state"];
    nlohmann::json::iterator it;
    BDD state = cudd_mgr->bddOne();
    for (it = init_data.begin(); it != init_data.end(); ++it) {
        state *= var_to_val_bdd[it.key()][it.value()];
    }
    init_state = bdd_to_cost_add(get_cudd_mgr(), state);
}

void DetTask::parse_goal_state(nlohmann::json& task_data) {
    auto init_data = task_data["goal_state"];
    nlohmann::json::iterator it;
    BDD state = cudd_mgr->bddOne();
    for (it = init_data.begin(); it != init_data.end(); ++it) {
        state *= var_to_val_bdd[it.key()][it.value()];
    }
    goal_state = bdd_to_cost_add(get_cudd_mgr(), state);
    goal_state += valid_values;
}

void DetTask::parse_actions(nlohmann::json& task_data) {
    auto init_data = task_data["actions"];
    nlohmann::json::iterator it;
    for (it = init_data.begin(); it != init_data.end(); ++it) {
        std::cout << it.key() << "..." << std::flush;
        std::string op_name = it.key();
        std::string tr = it.value()["Tr"];
        std::string tc = it.value()["Tc"];
        ADD action_add = add_creator.create_add(tr, *this);
        ADD cost_add = add_creator.create_add(tc, *this);
        // Its sufficent to encode invalid states in cost_add
        cost_add += valid_values;
        trs.emplace_back(op_name, action_add, cost_add, pre_vars, eff_vars,
                         get_cudd_mgr());
        long time_elapsed = Cudd_ReadElapsedTime(get_cudd_mgr()->getManager());
        long timeout = Cudd_ReadTimeLimit(get_cudd_mgr()->getManager());
        std::cout << "...overall time: " << time_elapsed / 1000.0;
        std::cout << " => Time left: " << (timeout - time_elapsed) / 1000.0
                  << "s\n"
                  << std::endl;

        // Dot last tr
        // dump_dot(trs.back().get_action_add(),
        //         trs.back().get_name() + "action.dot");
        // dump_dot(trs.back().get_cost_add(), trs.back().get_name() +
        // "cost.dot");
        // dump_dot(trs.back().get_tr_add(), trs.back().get_name() + "tr.dot");
    }
}

Cudd* DetTask::get_cudd_mgr() {
    return cudd_mgr.get();
}

ADD DetTask::create_add(std::string term) {
    return add_creator.create_add(term, *this);
}

ADD DetTask::get_var_ADD(std::string name) {
    if (var_ADD.find(name) == var_ADD.end()) {
        ADD res = cudd_mgr->plusInfinity();
        int cur_value = 0;
        // Create for every value an BDD representing the value (0, 1)
        // Then transform it to and cost ADD (inf, 0) + cur_value
        std::vector<int> bdd_vars = variables_names_to_bdd_indices[name];
        for (int val = 0; val < variable_names_to_domain_size[name]; val++) {
            ADD cost_add =
                bdd_to_cost_add(get_cudd_mgr(), var_to_val_bdd[name][val]);
            cost_add += cudd_mgr->constant(cur_value);
            res = res.Minimum(cost_add);
            cur_value++;
            // dump_dot(cost_add, name + "_" + std::to_string(val) + ".dot");
        }
        var_ADD[name] = res;
    }
    return var_ADD[name];
}

BDD DetTask::generate_bdd_var(std::string var_name, int value) {
    return generate_bdd_var(variables_names_to_bdd_indices[var_name], value);
}

BDD DetTask::generate_bdd_var(const std::vector<int>& bddVars, int value) {
    BDD res = cudd_mgr->bddOne();
    for (int v : bddVars) {
        if (value % 2) { // Check if the binary variable is asserted or negated
            res = res * cudd_mgr->bddVar(v);
        } else {
            res = res * (!cudd_mgr->bddVar(v));
        }
        value /= 2;
    }
    return res;
}

// TODO(speckd): if multivalued variables => this will fail!
void DetTask::dump_dot(cuddCPP::ADD add, std::string filename) {
    // Get char** for dot printing
    std::vector<char*> cstrings{};
    for (auto& string : variable_bdd_names) {
        cstrings.push_back(&string.front());
    }
    write_add_dot(get_cudd_mgr(), add, filename, cstrings.data());
}

void DetTask::reset(bool kill_mgr) {
    std::cout << "Reset Det Task." << std::endl;
    variable_names.clear();
    variable_bdd_names.clear();
    trs.clear();
    variable_order.clear();
    variable_names_to_index.clear();
    variable_names_to_domain_size.clear();
    for (auto& el : variables_names_to_bdd_indices) {
        el.second.clear();
    }
    variables_names_to_bdd_indices.clear();
    var_ADD.clear();
    for (auto& el : var_to_val_bdd) {
        el.second.clear();
    }
    var_to_val_bdd.clear();
    pre_vars.clear();
    eff_vars.clear();
    if (kill_mgr) {
        std::cout << "Reset (kill) Cudd Mgr." << std::endl;
        cudd_mgr.reset();
    }
}

void DetTask::mergeTrs(int max_time, int max_size) {
    std::cout << "Merging " << trs.size() << " action..." << std::flush;
    if (max_size <= 1 || trs.size() <= 1) {
        return;
    }
    std::vector<TransitionRelation> result;
    Stopwatch timer;
    timer.reset();
    while (trs.size() > 1 && (max_time == 0 || timer() < max_time)) {
        TransitionRelation t1 = trs.back();
        trs.pop_back();
        // t1 is already large enough!
        if (t1.get_tr_add().nodeCount() >= max_size) {
            result.push_back(t1);
        } else {
            TransitionRelation t2 = trs.back();
            trs.pop_back();
            t1.merge(t2, max_size);
            trs.insert(trs.begin(), t1);
        }
    }
    // Push all max size trs back into trs
    if (!result.empty()) {
        trs.insert(trs.end(), result.begin(), result.end());
    }
    std::cout << "to " << trs.size() << " TRs" << std::endl;
}

#ifndef TRANSITION_RELATION
#define TRANSITION_RELATION

#include <cuddObj.hh>
#include <set>
#include <string>

class TransitionRelation {
public:
    TransitionRelation(std::string op_name, cuddCPP::ADD action_add,
                       cuddCPP::ADD cost_add,
                       std::vector<cuddCPP::ADD>& swap_vars,
                       std::vector<cuddCPP::ADD>& swap_vars_p,
                       cuddCPP::Cudd* mgr);

    cuddCPP::ADD get_action_add() {
        return action_add;
    }

    cuddCPP::ADD get_cost_add() {
        return cost_add;
    }

    const cuddCPP::ADD get_tr_add() const {
        return tr;
    }

    std::string get_name() const {
        std::string res = "";
        for (auto& op_name : op_names) {
            res += op_name + "_";
        }
        return res;
    }

    const std::set<std::string>& get_op_names() const {
        return op_names;
    }

    cuddCPP::ADD image(cuddCPP::ADD from);
    cuddCPP::ADD preimage(cuddCPP::ADD from);

    // As all frame axioms are included and quantify always over all vars
    // we can skip the whole effvars merging. Should probably be fixed once the
    // first argument does not hold anymore
    bool merge(const TransitionRelation& t2, int max_nodes);

private:
    cuddCPP::ADD action_add;
    cuddCPP::ADD cost_add;
    cuddCPP::ADD tr;
    cuddCPP::ADD exists_vars;
    cuddCPP::ADD exists_bw_vars;
    std::vector<cuddCPP::ADD> swap_vars, swap_vars_p;
    std::set<std::string> op_names;
    cuddCPP::Cudd* mgr;
};

#endif /* TRANSITION_RELATION */

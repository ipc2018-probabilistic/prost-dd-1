#ifndef CUDD_UTIL_H
#define CUDD_UTIL_H

#include <cuddObj.hh>
#include <string>

cuddCPP::ADD bdd_to_cost_add(cuddCPP::Cudd* mgr, const cuddCPP::BDD& in);

cuddCPP::ADD partialCmpl(cuddCPP::Cudd* mgr, const cuddCPP::ADD& in);

cuddCPP::ADD remove_cost(cuddCPP::Cudd* mgr, const cuddCPP::ADD& in);

void write_add_dot(cuddCPP::Cudd* mgr, cuddCPP::ADD add, std::string filename,
                   char** var_names = nullptr, bool dump_info = true);

void write_bdd_dot(cuddCPP::Cudd* mgr, cuddCPP::BDD bdd, std::string filename,
                   char** var_names = nullptr, bool dump_info = true);

#endif /* CUDD_UTIL_H */
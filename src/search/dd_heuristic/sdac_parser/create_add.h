// Copyright 27.03.2018, University of Freiburg,
// Author: David Speck <speckd>.

#ifndef SDAC_PARSER_CREATE_ADD_H_
#define SDAC_PARSER_CREATE_ADD_H_

#include "catamorph.h"
#include "expression.h"
#include "parser.h"
#include <cuddObj.hh>
#include <experimental/type_traits>
#include <functional>
#include <numeric>
#include <string>
#include <vector>
#include <iostream>

class DetTask;

class CreateADD {
private:
    std::map<std::string, cuddCPP::ADD>
        look_up; // Used if we have identical sdac
    InfixParser infix_parser;
    PrefixParser prefix_parser;
    int round_dez;

    auto create_add_alg(DetTask& task) {
        return [&task,
                this](expression_r<cuddCPP::ADD> const& e) -> cuddCPP::ADD {
            if (!timeLeft(task)) {
                std::cout << "Timeout!" << std::endl;
                throw std::runtime_error("Time (build up) is over!");
            }
            if (const NBR* cst = Factories::get_as_cst(e)) {
                return createADD(cst, task).RoundOff(round_dez);
            }
            if (auto* var = Factories::get_as_var(e)) {
                std::string name(*var);
                return createADD(name, task).RoundOff(round_dez);
            }
            if (auto* o = Factories::get_as_add(e)) {
                return std::accumulate(o->rands().begin() + 1, o->rands().end(),
                                       o->rands().front(),
                                       std::plus<cuddCPP::ADD>())
                    .RoundOff(round_dez);
            }
            if (auto* o = Factories::get_as_sub(e)) {
                return std::accumulate(o->rands().begin() + 1, o->rands().end(),
                                       o->rands().front(),
                                       std::minus<cuddCPP::ADD>())
                    .RoundOff(round_dez);
            }
            if (auto* o = Factories::get_as_mul(e)) {
                return std::accumulate(o->rands().begin() + 1, o->rands().end(),
                                       o->rands().front(),
                                       std::multiplies<cuddCPP::ADD>())
                    .RoundOff(round_dez);
            }
            if (auto* o = Factories::get_as_div(e)) {
                return std::accumulate(o->rands().begin() + 1, o->rands().end(),
                                       o->rands().front(),
                                       [](cuddCPP::ADD x, cuddCPP::ADD y) {
                                           return x.Divide(y);
                                       })
                    .RoundOff(round_dez);
            }
            if (auto* o = Factories::get_as_greater(e)) {
                assert(o->rands().size() == 2);
                return o->rands()[0].GreaterThan(o->rands()[1]);
            }
            if (auto* o = Factories::get_as_greater_equals(e)) {
                assert(o->rands().size() == 2);
                return o->rands()[0].GreaterThanEquals(o->rands()[1]);
            }
            if (auto* o = Factories::get_as_lesser(e)) {
                assert(o->rands().size() == 2);
                return o->rands()[0].LessThan(o->rands()[1]);
            }
            if (auto* o = Factories::get_as_lesser_equals(e)) {
                assert(o->rands().size() == 2);
                return o->rands()[0].LessThanEquals(o->rands()[1]);
            }
            if (auto* o = Factories::get_as_and(e)) {
                return std::accumulate(o->rands().begin() + 1, o->rands().end(),
                                       o->rands().front(),
                                       std::bit_and<cuddCPP::ADD>())
                    .RoundOff(round_dez);
            }
            if (auto* o = Factories::get_as_equals(e)) {
                return std::accumulate(
                    o->rands().begin() + 1, o->rands().end(),
                    o->rands().front(),
                    [](cuddCPP::ADD x, cuddCPP::ADD y) { return x.Equals(y); });
            }
            if (auto* o = Factories::get_as_or(e)) {
                return std::accumulate(o->rands().begin() + 1, o->rands().end(),
                                       o->rands().front(),
                                       std::bit_or<cuddCPP::ADD>())
                    .RoundOff(round_dez);
            }
            if (auto* o = Factories::get_as_not(e)) {
                assert(o->rands().size() == 1);
                return o->rands()[0].Cmpl().RoundOff(round_dez);
            }
            if (auto* o = Factories::get_as_pow(e)) {
                assert(o->rands().size() == 2);
                return o->rands()[0].Pow(o->rands()[1]).RoundOff(round_dez);
            }
            throw std::logic_error("Unknown Operator in Apply");
        };
    }

    bool timeLeft(DetTask& task);
    const cuddCPP::ADD createADD(const NBR* cst, DetTask& task);
    const cuddCPP::ADD createADD(const std::string& name, DetTask& task);
    const cuddCPP::ADD getABS(std::vector<cuddCPP::ADD> elements,
                              DetTask& task);
    cuddCPP::ADD create_add(Expression const& expr, DetTask& task) {
        return Catamorph::cata<cuddCPP::ADD>(
            [&task, this](expression_r<cuddCPP::ADD> const& expr_r)
                -> cuddCPP::ADD { return create_add_alg(task)(expr_r); },
            expr);
    }

public:
    CreateADD() : round_dez(2){};
    CreateADD(int round_dez) : round_dez(round_dez){};
    cuddCPP::ADD create_add(const std::string cost_term, DetTask& task);
};

#endif // SDAC_PARSER_CREATE_ADD_H_

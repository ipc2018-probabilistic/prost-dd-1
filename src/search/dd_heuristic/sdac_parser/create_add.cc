#include "create_add.h"
#include "../../utils/system_utils.h"
#include "../det_task.h"
#include <iostream>

using namespace cuddCPP;


bool CreateADD::timeLeft(DetTask& task) {
  long time_elapsed = Cudd_ReadElapsedTime(task.get_cudd_mgr()->getManager());
  long timeout = Cudd_ReadTimeLimit(task.get_cudd_mgr()->getManager());
  return timeout - time_elapsed > 0;
}

const ADD CreateADD::createADD(const NBR* cst, DetTask& task) {
    return task.get_cudd_mgr()->constant((double)*cst);
}
const ADD CreateADD::createADD(const std::string& name, DetTask& task) {
    return task.get_var_ADD(name);
}

const ADD CreateADD::getABS(std::vector<cuddCPP::ADD> elements, DetTask& task) {
    ADD pos = std::accumulate(elements.begin() + 1, elements.end(),
                              elements.front(), std::plus<ADD>());
    ADD neg = pos * task.get_cudd_mgr()->constant(-1);
    return pos.Maximum(neg);
}

ADD CreateADD::create_add(const std::string cost_term, DetTask& task) {
    if (look_up.count(cost_term)) {
        return look_up[cost_term];
    }

    Expression expression;
    try {
        expression = prefix_parser.parse(cost_term);
    } catch (std::exception const& infix_exception) {
        try {
            expression = infix_parser.parse(cost_term);
        } catch (std::exception const& prefix_exception) {
            std::cout << "Parser error: " << std::endl;
            std::cout << "Infix parser error: " << infix_exception.what()
                      << std::endl;
            std::cout << "Prefix parser error: " << prefix_exception.what()
                      << std::endl;
            throw std::invalid_argument("Invalid argument for parser.");
        }
    }
    // Printer::print(expression);
    ADD cost = create_add(expression, task);
    // write_dd(sV->mgr(), cost.getNode(), Printer::as_string(expression));
    look_up[cost_term] = cost;
    return cost;
}

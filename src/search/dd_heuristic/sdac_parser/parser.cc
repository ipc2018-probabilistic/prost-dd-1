#include "parser.h"
#include "expression.h"
#include "factories.h"

#include <assert.h>
#include <iostream>
#include <regex>

using std::string;
using std::vector;
using std::shared_ptr;
using std::stod;

Token Lexer::getNextToken() {
    if (returnPrevToken) {
        returnPrevToken = false;
        return previousToken;
    }

    // trim surrounding parantheses
    input.erase(input.begin(),
                std::find_if(input.begin(), input.end(),
                             [](int ch) { return !std::isspace(ch); }));
    input.erase(std::find_if(input.rbegin(), input.rend(),
                             [](int ch) { return !std::isspace(ch); })
                    .base(),
                input.end());

    std::regex addRegex("^\\+");                // Regex for arithmetic +
    std::regex subRegex("^-");                  // Regex for arithmetic -
    std::regex multRegex("^\\*");               // Regex for arithmetic *
    std::regex divRegex("^\\/");                // Regex for arithmetic /
    std::regex powRegex("^\\^");                // Regex for arithmetic power of
    std::regex greaterRegex("^\\>");            // Regex for > comparison
    std::regex lesserRegex("^\\<");             // Regex for < comparison
    std::regex greater_equals_Regex("^\\>\\="); // Regex for >= comparison
    std::regex lesser_equals_Regex("^\\<\\=");  // Regex for <= comparison
    // Regex for constant numbers.
    // Note that we use the passive group (?:subpattern) here, so that we only
    // have one backreference for the whole constant and not multiple
    // backreferences for individual constant parts
    std::regex constantRegex(
        "^((?:[[:digit:]]+)(?:\\.(?:(?:[[:digit:]]+)?))?)");
    std::regex lParenRegex("^\\("); // Regex for (
    std::regex rParenRegex("^\\)"); // Regex for )
    // Variables contain letters, numbers or "_",
    // but must not start with a number
    std::regex varRegex("^((?:[[:alpha:]_]+)(?:[[:alnum:]_]*))");

    // Iverson regex
    std::regex lSqrBrackRegex("^\\["); // Regex for [
    std::regex rSqrBrackRegex("^\\]"); // Regex for ]
    std::regex andRegex("^\\&\\&");    // Regex for && (Logical and)
    std::regex orRegex("^\\|\\|");     // Regex for || (Logical or)
    std::regex equalsRegex("^\\=\\="); // Regex for == (Logical equals)
    std::regex notRegex("^\\!");       // Regex for ! (Logical not)

    Token token;
    std::smatch match;
    if (std::regex_search(input, match, addRegex)) {
        token.type = Type::OP;
        token.value = "+";
        input = input.substr(match.position() + match.length());
    } else if (std::regex_search(input, match, subRegex)) {
        token.type = Type::OP;
        token.value = "-";
        input = input.substr(match.position() + match.length());
    } else if (std::regex_search(input, match, multRegex)) {
        token.type = Type::OP;
        token.value = "*";
        input = input.substr(match.position() + match.length());
    } else if (std::regex_search(input, match, divRegex)) {
        token.type = Type::OP;
        token.value = "/";
        input = input.substr(match.position() + match.length());
    } else if (std::regex_search(input, match, powRegex)) {
        token.type = Type::OP;
        token.value = "^";
        input = input.substr(match.position() + match.length());
    // greater_equals / lesser_equals has to be checked before greater/lesser,
    // otherwise >= would be parsed to >,= instead to >=.
    } else if (std::regex_search(input, match, greater_equals_Regex)) {
        token.type = Type::OP;
        token.value = ">=";
        input = input.substr(match.position() + match.length());
    } else if (std::regex_search(input, match, lesser_equals_Regex)) {
        token.type = Type::OP;
        token.value = "<=";
        input = input.substr(match.position() + match.length());
    } else if (std::regex_search(input, match, greaterRegex)) {
        token.type = Type::OP;
        token.value = ">";
        input = input.substr(match.position() + match.length());
    } else if (std::regex_search(input, match, lesserRegex)) {
        token.type = Type::OP;
        token.value = "<";
        input = input.substr(match.position() + match.length());
    } else if (std::regex_search(input, match, andRegex)) {
        token.type = Type::OP;
        token.value = "&&";
        input = input.substr(match.position() + match.length());
    } else if (std::regex_search(input, match, orRegex)) {
        token.type = Type::OP;
        token.value = "||";
        input = input.substr(match.position() + match.length());
    } else if (std::regex_search(input, match, equalsRegex)) {
        token.type = Type::OP;
        token.value = "==";
        input = input.substr(match.position() + match.length());
    } else if (std::regex_search(input, match, notRegex)) {
        token.type = Type::OP;
        token.value = "!";
        input = input.substr(match.position() + match.length());
    } else if (std::regex_search(input, match, constantRegex)) {
        token.type = Type::CONST;
        token.value = input.substr(0, match.position() + match.length());
        input = input.substr(match.position() + match.length());
    } else if (std::regex_search(input, match, lParenRegex)) {
        token.type = Type::LPAREN;
        input = input.substr(match.position() + match.length());
    } else if (std::regex_search(input, match, rParenRegex)) {
        token.type = Type::RPAREN;
        input = input.substr(match.position() + match.length());
    } else if (std::regex_search(input, match, lSqrBrackRegex)) {
        token.type = Type::LSQRBRACK;
        input = input.substr(match.position() + match.length());
    } else if (std::regex_search(input, match, rSqrBrackRegex)) {
        token.type = Type::RSQRBRACK;
        input = input.substr(match.position() + match.length());
    } else if (std::regex_search(input, match, varRegex)) {
        token.type = Type::VAR;
        token.value = input.substr(0, match.position() + match.length());
        input = input.substr(match.position() + match.length());
    } else if (input.empty()) {
        token.type = Type::END;
    }
    if (token.type == Type::INVALID) {
        string error = "Illegal token at start of substring: \"" + input;
        error += "\"";
        throw std::invalid_argument(error);
        exit(0);
    }
    previousToken = token;
    return token;
}

Expression PrefixParser::parse(string const& input) const {
    // For user convenience we surround the input string with parantheses
    string expr = "(" + input + ")";
    Lexer lexer(expr);
    return parseExpression(lexer);
}

Expression PrefixParser::parseExpression(Lexer& lexer) const {
    Token token = lexer.getNextToken();
    switch (token.type) {
    case Type::CONST:
        return Factories::cst(stod(token.value));
        break;
    case Type::VAR:
        return Factories::var(token.value);
        break;
    case Type::OP:
        lexer.revert();
        return parseOpExpression(lexer);
        break;
    case Type::LPAREN: {
        string const beforeRParen = lexer.input;
        Expression expression = parseExpression(lexer);
        token = lexer.getNextToken();
        if (token.type != Type::RPAREN) {
            throw std::invalid_argument("Missing ) for substring " +
                                        beforeRParen);
        }
        return expression;
        break;
    }
    case Type::RPAREN:
        throw std::invalid_argument("No matching ( for substring " +
                                    lexer.input);
        break;
    case Type::LSQRBRACK: {
        string const beforeRParen = lexer.input;
        Expression expression = parseExpression(lexer);
        token = lexer.getNextToken();
        if (token.type != Type::RSQRBRACK) {
            throw std::invalid_argument("Missing ] for substring " +
                                        beforeRParen);
        }
        return expression;
        break;
    }
    case Type::RSQRBRACK:
        throw std::invalid_argument("No matching [ for substring " +
                                    lexer.input);
    default:
        throw std::invalid_argument("Illegal expression: " + lexer.input);
        break;
    }
}

Expression PrefixParser::parseOpExpression(Lexer& lexer) const {
    Token token = lexer.getNextToken();
    assert(token.type == Type::OP);
    string opType = token.value;

    string const beforeRParen = lexer.input;
    token = lexer.getNextToken();
    vector<Expression> exprs;
    while (token.type != Type::RPAREN) {
        if (token.type == Type::END) {
            throw std::invalid_argument("Missing ) for substring " +
                                        beforeRParen);
        }
        lexer.revert();
        exprs.push_back(parseExpression(lexer));
        token = lexer.getNextToken();
    }
    if (exprs.empty()) {
        throw std::invalid_argument("Empty operator: " + opType + " before" +
                                    beforeRParen);
    }
    // Revert because the ")" is again checked in the "(" expression part
    lexer.revert();
    if (opType == "+") {
        return Factories::add(exprs);
    } else if (opType == "-") {
        if (exprs.size() == 1) {
            exprs.insert(exprs.begin(), Factories::cst(0));
        }
        return Factories::sub(exprs);
    } else if (opType == "*") {
        return Factories::mul(exprs);
    } else if (opType == "^") {
        return Factories::pow(exprs);
    } else if (opType == ">") {
        return Factories::greater(exprs);
    } else if (opType == "<") {
        return Factories::lesser(exprs);
    } else if (opType == ">=") {
        return Factories::greater_equals(exprs);
    } else if (opType == "<=") {
        return Factories::lesser_equals(exprs);
    } else if (opType == "&&") {
        return Factories::land(exprs);
    } else if (opType == "||") {
        return Factories::lor(exprs);
    } else if (opType == "==") {
        return Factories::equals(exprs);
    } else if (opType == "!") {
        return Factories::lnot(exprs);
    } else {
        throw std::invalid_argument("Unknown operator:" + opType);
    }
}

///////////// INFIX PARSER /////////////////////

bool InfixParser::isBinaryOperator(Token const& token) {
    return (token.value == "+" || token.value == "-" || token.value == "*" ||
            token.value == "/" || token.value == ">" || token.value == ">=" ||
            token.value == "^" || token.value == "<" || token.value == "<=");
}

bool InfixParser::isUnaryOperator(Token const& token) {
    return token.value == "-";
}

bool InfixParser::hasHigherPrecedence(Token const& first, Token const& second) {
    if (!second.binary && second.value == "-") {
        return false;
    }
    if (!first.binary && first.value == "-") {
        return true;
    }

    return op_precedence.at(first.value) > op_precedence.at(second.value);
}

bool InfixParser::isLogicalBinaryOperator(Token const& token) {
    return (token.value == "&&" || token.value == "||" || token.value == "==");
}

bool InfixParser::isLogicalUnaryOperator(Token const& token) {
    return (token.value == "!");
}

void InfixParser::expect(Type type, Lexer& lexer) {
    if (type == next.type) {
        consume(lexer);
    } else {
        throw std::invalid_argument("Parser error : expected " +
                                    std::to_string(type) + " was " +
                                    std::to_string(next.type));
    }
}

void InfixParser::popOperator() {
    if ((isBinaryOperator(operators.top()) ||
         isLogicalBinaryOperator(operators.top())) &&
        operators.top().binary) {
        Expression lhs = operands.top();
        operands.pop();
        Expression rhs = operands.top();
        operands.pop();
        operands.push(createExpression(rhs, operators.top(), lhs));
        operators.pop();
    } else {
        Expression lhs = operands.top();
        operands.pop();
        operands.push(createUnaryExpression(lhs, operators.top()));
        operators.pop();
    }
}

void InfixParser::pushOperator(Token const& token) {
    while (hasHigherPrecedence(operators.top(), token)) {
        popOperator();
    }
    operators.push(token);
}

void InfixParser::consume(Lexer& lexer) {
    next = lexer.getNextToken();
}

Expression InfixParser::parse(string const& input) {
    Lexer lexer(input);
    next = lexer.getNextToken();
    Token sentinel = Token(Type::OP, "sentinel");
    operators.push(sentinel);
    E(lexer);
    expect(Type::END, lexer);

    return operands.top();
}

void InfixParser::E(Lexer& lexer) {
    if (next.type == Type::LSQRBRACK) {
        LogicEXP(lexer);
    } else {
        P(lexer);
    }
    while (isBinaryOperator(next)) {
        pushOperator(next);
        consume(lexer);
        P(lexer);
    }

    while (operators.top().value != "sentinel") {
        popOperator();
    }
}

void InfixParser::P(Lexer& lexer) {
    if (next.type == Type::VAR) {
        // std::cout<< "VAR: "<<next.value<<std::endl;
        operands.push(Factories::var(next.value));
        consume(lexer);
    } else if (next.type == Type::CONST) {
        // std::cout<< "Const: "<<next.value<<std::endl;
        operands.push(Factories::cst(stod(next.value)));
        consume(lexer);
    } else if (next.type == Type::LPAREN) {
        // std::cout<< "LPAREN: "<<next.value<<std::endl;
        consume(lexer);
        Token sentinel = Token(Type::OP, "sentinel");
        operators.push(sentinel);
        E(lexer);
        expect(Type::RPAREN, lexer);
        operators.pop();
    } else if (isUnaryOperator(next)) {
        // std::cout<< "unary: "<<next.value<<std::endl;
        next.binary = false;
        pushOperator(next);
        consume(lexer);
        P(lexer);
    } else if (next.type == Type::LSQRBRACK) {
        // std::cout<< "L Exp: "<<next.value<<std::endl;
        LogicEXP(lexer);
    } else {
        throw std::invalid_argument(
            "P Unknown Token \"" + std::to_string(next.type) +
            "\" with value: \"" + next.value + "\" at " + lexer.input);
    }
}

void InfixParser::LogicEXP(Lexer& lexer) {
    if (next.type != Type::LSQRBRACK) {
        throw std::invalid_argument("expected [");
    }
    Token sentinel = Token(Type::OP, "sentinel");
    operators.push(sentinel);
    consume(lexer); // consume [
    LP(lexer);
    while (isLogicalBinaryOperator(next)) {
        pushOperator(next);
        consume(lexer);
        LP(lexer);
    }
    expect(Type::RSQRBRACK, lexer);
    while (operators.top().value != "sentinel") {
        popOperator();
    }
    operators.pop(); // remove [
}

void InfixParser::LP(Lexer& lexer) {
    if (next.type == Type::VAR) {
        operands.push(Factories::var(next.value));
        consume(lexer);
    } else if (next.type == Type::CONST) {
        operands.push(Factories::cst(stod(next.value)));
        consume(lexer);
    } else if (isLogicalUnaryOperator(next)) {
        next.binary = false;
        pushOperator(next);
        consume(lexer);
        LP(lexer);
    } else if (next.type == Type::LSQRBRACK) {
        LogicEXP(lexer);
    } else if (next.type == Type::LPAREN) {
        consume(lexer);
        Token sentinel = Token(Type::OP, "sentinel");
        operators.push(sentinel);
        E(lexer);
        expect(Type::RPAREN, lexer);
        operators.pop();
    } else {
        throw std::invalid_argument(
            " LP Unknown Token \"" + std::to_string(next.type) +
            "\" with value \"" + next.value + "\" at " + lexer.input);
    }
}

Expression InfixParser::createExpression(Expression const& lhs, Token op,
                                         Expression const& rhs) const {
    vector<Expression> exprs{lhs, rhs};
    if (op.value == "+") {
        return Factories::add(exprs);
    } else if (op.value == "-") {
        return Factories::sub(exprs);
    } else if (op.value == "*") {
        return Factories::mul(exprs);
    } else if (op.value == "/") {
        return Factories::div(exprs);
    } else if (op.value == "^") {
        return Factories::pow(exprs);
    } else if (op.value == ">") {
        return Factories::greater(exprs);
    } else if (op.value == "<") {
        return Factories::lesser(exprs);
    } else if (op.value == ">=") {
        return Factories::greater_equals(exprs);
    } else if (op.value == "<=") {
        return Factories::lesser_equals(exprs);
    } else if (op.value == "&&") {
        return Factories::land(exprs);
    } else if (op.value == "||") {
        return Factories::lor(exprs);
    } else if (op.value == "==") {
        return Factories::equals(exprs);
    } else {
        throw std::invalid_argument("Unknown binary operator:" + op.value);
    }
}

Expression InfixParser::createUnaryExpression(Expression const& exp,
                                              Token op) const {
    if (op.value == "!") {
        vector<Expression> exprs{exp};
        return Factories::lnot(exprs);
    } else if (op.value == "-") {
        vector<Expression> exprs{Factories::cst(0), exp};
        return Factories::sub(exprs);
    } else {
        throw std::invalid_argument("Unknown unary operator:" + op.value);
    }
}

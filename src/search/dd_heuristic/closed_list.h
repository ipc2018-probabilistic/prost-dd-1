// Copyright 16.03.2018, University of Freiburg,
// Author: David Speck <speckd>.

#ifndef DD_HEURISTIC_CLOSED_LIST_H_
#define DD_HEURISTIC_CLOSED_LIST_H_

#include <cuddObj.hh>
#include <limits>

class ClosedList {
protected:
    cuddCPP::Cudd* mgr;
    // A big closed list (inverted) to remove states from the open list
    cuddCPP::ADD closed;
    // A closed list for every layer (necessary for plan reconsturction)
    std::vector<cuddCPP::ADD> closed_layers;

public:
    void init(cuddCPP::Cudd* mgr,
              double value = std::numeric_limits<double>::infinity());

    void insert(const cuddCPP::ADD in);

    const cuddCPP::ADD get_last_layer() const {
        return closed_layers.back();
    }

    const cuddCPP::ADD get_layer(int i) const {
        return closed_layers[i];
    }

    size_t get_num_layer() {
        return closed_layers.size();
    }
};

#endif // DD_HEURISTIC_LIST_H_

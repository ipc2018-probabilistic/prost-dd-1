#include "transition_relation.h"
#include "cudd_util.h"
#include "det_task.h"
#include <iostream>
#include <set>

using namespace cuddCPP;

TransitionRelation::TransitionRelation(std::string op_name, ADD action_add,
                                       ADD cost_add,
                                       std::vector<ADD>& swap_vars,
                                       std::vector<ADD>& swap_vars_p, Cudd* mgr)
    : action_add(action_add),
      cost_add(cost_add),
      swap_vars(swap_vars),
      swap_vars_p(swap_vars_p),
      mgr(mgr) {
    op_names.insert(op_name);
    // Conjunction to get the cube for quantifing
    exists_vars = swap_vars[0];
    exists_bw_vars = swap_vars_p[0];
    for (size_t i = 1; i < swap_vars.size(); ++i) {
        exists_vars *= swap_vars[i];
        exists_bw_vars *= swap_vars_p[i];
    }

    // action add is a 0-1 ADD. by BDDThreshold we simply convert it to an BDD
    // TODO(speckd) this should go more eff
    // auto max_bdd = Cudd_V(action_add.FindMax().getNode());
    // write_add_dot(mgr, this->action_add, get_name() + "__p.dot", nullptr,
    // true);
    this->action_add = this->action_add.BddStrictThreshold(0).Add();
    tr = this->action_add * mgr->plusInfinity();
    tr = partialCmpl(mgr, tr);
    tr += cost_add;
}

ADD TransitionRelation::image(ADD from) {
    ADD aux = from + tr;
    // image
    ADD tmp = aux.MinAbstract(exists_vars);
    ADD res = tmp.SwapVariables(swap_vars, swap_vars_p);
    return res;
}

ADD TransitionRelation::preimage(ADD from) {
    ADD tmp = from.SwapVariables(swap_vars, swap_vars_p) + tr;
    ADD res = tmp.MinAbstract(exists_bw_vars);
    return res;
}

bool TransitionRelation::merge(const TransitionRelation& t2,
                               int /*max_nodes*/) {
    std::vector<int> newEffVars;

    ADD newTr = tr;
    ADD newTr2 = t2.get_tr_add();

    // We can simply combine all TRs because here frame axioms are always fully
    // encoded
    newTr = newTr.Minimum(newTr2);

    /*if (newTr.nodeCount() > max_nodes) {
        std::cout << "TR size exceeded: " << newTr.nodeCount() << ">"
                  << max_nodes << std::endl;
        return false;
    }*/

    // Able to merge
    tr = newTr;

    op_names.insert(t2.get_op_names().begin(), t2.get_op_names().end());
    return true;
}

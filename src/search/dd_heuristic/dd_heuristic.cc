// Copyright 04.04.2018, University of Freiburg,
// Author: David Speck <speckd>.

#include "dd_heuristic.h"
#include "../utils/stopwatch.h"
#include "det_task.h"
#include <iostream>
#include <limits>

using namespace cuddCPP;

void DDHeuristic::compute_dd_heuristic(DetTask& task, int horizon) {
    vars_to_indices = task.get_variables_names_to_bdd_indices();
    variable_names = task.get_original_variable_names();
    num_bdd_vars = task.get_num_of_bdd_vars();
    sym_state = std::vector<int>(num_bdd_vars, 0);

    layers.init(task.get_cudd_mgr());
    layers.insert(task.get_goal_state());
    Stopwatch timer;
    // Dummy value for already reached goal
    infinite_heuristic_values.push_back(0);
    worst_infinite_heuristic_value = -std::numeric_limits<double>::infinity();

    // task.dump_dot(layers.get_last_layer(),
    //              "layer" + std::to_string(0) + ".dot");

    for (int i = 0; i < horizon; i++) {
        std::cout << "Plan step " << i + 1 << "/" << horizon << "..."
                  << std::endl;
        ADD expand = layers.get_last_layer();
        ADD new_states = task.get_cudd_mgr()->plusInfinity();
        for (auto& tr : task.get_trs()) {
            new_states = new_states.Minimum(tr.preimage(expand));
        }
        layers.insert(new_states);

        // task.dump_dot(layers.get_last_layer(),
        //                  "layer" + std::to_string(i + 1) + ".dot");
        // Store worst value per layer in case of unseen states
        ADD negative_layer = -new_states;
        BDD pruned_layer = negative_layer.BddStrictThreshold(
            -std::numeric_limits<double>::infinity());
        negative_layer *= pruned_layer.Add();
        infinite_heuristic_values.push_back(
            -Cudd_V(negative_layer.FindMin().getNode()));

        // Collect worst
        worst_infinite_heuristic_value =
            worst_infinite_heuristic_value < infinite_heuristic_values.back()
                ? infinite_heuristic_values.back()
                : worst_infinite_heuristic_value;

        long time_elapsed =
            Cudd_ReadElapsedTime(task.get_cudd_mgr()->getManager());
        long timeout = Cudd_ReadTimeLimit(task.get_cudd_mgr()->getManager());
        std::cout << "...worst value: " << infinite_heuristic_values.back()
                  << std::endl;
        std::cout << "...overall worst value: "
                  << worst_infinite_heuristic_value << std::endl;
        std::cout << "...overall time: " << time_elapsed / 1000.0;
        std::cout << " => Time left: " << (timeout - time_elapsed) / 1000.0
                  << "s\n"
                  << std::endl;
    }
}

double DDHeuristic::get_h(std::vector<int>& state, int step_remaining) {
    if (layers.get_num_layer() <= 0) {
        return 0; // We never started search
    }

    // Incomplete build up => the value from the last layer or its worst
    if (step_remaining >= layers.get_num_layer()) {
        return get_h(state, layers.get_num_layer() - 1);
    }

    // Create state with correct var order
    // std::cout << "\n\n" << std::endl;
    for (size_t i = 0; i < state.size(); i++) {
        int value = state[i];
        // std::cout << i << ": " << variable_names[i] << " = " << value <<
        // std::endl;
        for (int v : vars_to_indices[variable_names[i]]) {
            // Check if the binary variable is set
            sym_state[v] = (value % 2) ? 1 : 0;
            // std::cout << "Set " << v << " to " << sym_state[v] << std::endl;
            value /= 2;
        }
    }

    double h_value =
        Cudd_V(layers.get_layer(step_remaining).Eval(&sym_state[0]).getNode());
    // TODO think about a better heuristic for states we have no information on
    if (h_value == std::numeric_limits<double>::infinity()) {
        h_value = infinite_heuristic_values[step_remaining];
    }
    // std::cout << h_value << std::endl;
    return h_value;
}

// Copyright 16.03.2018, University of Freiburg,
// Author: David Speck <speckd>.

#include "closed_list.h"
#include <algorithm>

using namespace cuddCPP;

void ClosedList::init(Cudd* mgr, double value) {
    this->mgr = mgr;
    closed = mgr->constant(value);
}

void ClosedList::insert(const ADD in) {
    closed_layers.push_back(in);
    closed = closed.Minimum(in);
}
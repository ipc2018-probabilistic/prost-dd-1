#include "cudd_util.h"
#include <cuddObj.hh>
#include <iostream>
#include <limits>

using namespace cuddCPP;

cuddCPP::ADD bdd_to_cost_add(Cudd* mgr, const BDD& in) {
    return (!in).Add() * mgr->plusInfinity();
}

ADD partialCmpl(Cudd* mgr, const ADD& in) {
    BDD bComp = !in.BddThreshold(std::numeric_limits<double>::infinity());
    return bComp.Add() * mgr->plusInfinity();
}

ADD remove_cost(cuddCPP::Cudd* mgr, const cuddCPP::ADD& in) {
    return partialCmpl(mgr, partialCmpl(mgr, in));
}

void write_add_dot(Cudd* mgr, ADD add, std::string filename, char** var_names,
                   bool dump_info) {
    if (dump_info)
        std::cout << "Dump ADD to " << filename << " as dot file..."
                  << std::flush;
    FILE* outfile; // output file pointer for .dot file
    outfile = fopen(filename.c_str(), "w");
    DdNode** ddnodearray = (DdNode**)malloc(
        sizeof(add.getNode())); // initialize the function array
    ddnodearray[0] = add.getNode();
    Cudd_DumpDot(mgr->getManager(), 1, ddnodearray, var_names, NULL,
                 outfile); // dump the function to .dot file
    free(ddnodearray);
    fclose(outfile); // close the file */
    if (dump_info)
        std::cout << "done!" << std::endl;
}

void write_bdd_dot(Cudd* mgr, BDD bdd, std::string filename, char** var_names,
                   bool dump_info) {
    if (dump_info)
        std::cout << "Dump BDD to " << filename << " as dot file..."
                  << std::flush;
    write_add_dot(mgr, bdd.Add(), filename, var_names, false);
    if (dump_info)
        std::cout << "done!" << std::endl;
}

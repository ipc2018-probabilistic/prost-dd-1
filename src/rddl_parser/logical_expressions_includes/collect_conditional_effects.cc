void LogicalExpression::collect_conditional_effects(
    map<int, int>& /*conditions*/) {
    std::cerr << "Error: called collect conditional effects on unsupported "
                 "expression."
              << std::endl;
    print(std::cerr);
    std::cerr << std::endl;
    assert(false);
}

/*****************************************************************
                           Atomics
*****************************************************************/
void StateFluent::collect_conditional_effects(map<int, int>& conditions) {
    conditions[index] = 1;
}

void NumericConstant::collect_conditional_effects(map<int, int>& /*c*/) {
    assert(value == 1.0);
    // If the effect holds unconditionally, we do not have to print out effect
    // conditions
}

/*****************************************************************
                           Connectives
*****************************************************************/
void Conjunction::collect_conditional_effects(map<int, int>& conditions) {
    for (LogicalExpression* expr : exprs) {
        expr->collect_conditional_effects(conditions);
    }
}

/*****************************************************************
                          Unaries
*****************************************************************/
void Negation::collect_conditional_effects(map<int, int>& conditions) {
    auto sf = dynamic_cast<StateFluent*>(expr);
    assert(sf);
    conditions[sf->index] = 0;
}


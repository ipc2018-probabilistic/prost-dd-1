#include <algorithm>

bool LogicalExpression::operator==(LogicalExpression const& /*rhs*/) {
    return false;
}

/*****************************************************************
                           Atomics
*****************************************************************/

bool ParametrizedVariable::operator==(LogicalExpression const& rhs) {
    const ParametrizedVariable* p = 
        dynamic_cast<const ParametrizedVariable*>(&rhs);
    if (!p) return false;
    if (variableName != p->variableName) return false;
    if (variableType != p->variableType) return false;
    if (valueType->name != p->valueType->name) return false;
    if (!MathUtils::doubleIsEqual(initialValue, p->initialValue)) return false;

    for (Parameter* param : params) {
        for (Parameter* rhsParam : p->params) {
            if (param->name != rhsParam->name) return false;
            if (param->type->name != rhsParam->type->name) return false;
        }
    }
    return true;
}


bool StateFluent::operator==(LogicalExpression const& rhs) {
    const StateFluent* sf = dynamic_cast<const StateFluent*>(&rhs);
    if (!sf) return false;
    if (index == -1) return false;
    return sf->index == index;
}

bool ActionFluent::operator==(LogicalExpression const& rhs) {
    const ActionFluent* af = dynamic_cast<const ActionFluent*>(&rhs);
    if (!af) return false;
    return af->index == index;
}

bool NumericConstant::operator==(LogicalExpression const& rhs) {
    const NumericConstant* c = dynamic_cast<const NumericConstant*>(&rhs);
    if (!c) return false;
    return MathUtils::doubleIsEqual(c->value, value);
}



/*****************************************************************
  Connectives
 *****************************************************************/
bool Conjunction::operator==(LogicalExpression const& rhs) {
    const Conjunction* conj = dynamic_cast<const Conjunction*>(&rhs);
    if (!conj) return false;
    if (exprs.size() != conj->exprs.size()) {
        return false;
    }

    for (LogicalExpression* expr : conj->exprs) {
        // Find element of rhs in lhs
        auto find = std::find_if(exprs.begin(), exprs.end(), 
                [expr] (LogicalExpression* e) -> bool { return (*expr == *e);});
        if (find == exprs.end()) return false;
    }
    return true;
}

bool Disjunction::operator==(LogicalExpression const& rhs) {
    const Disjunction* disj = dynamic_cast<const Disjunction*>(&rhs);
    if (!disj) return false;
    if (exprs.size() != disj->exprs.size()) {
        return false;
    }

    for (LogicalExpression* expr : disj->exprs) {
        // Find element of rhs in lhs
        auto find = std::find_if(exprs.begin(), exprs.end(), 
                [expr] (LogicalExpression* e) -> bool { return (*expr == *e);});
        if (find == exprs.end()) return false;
    }
    return true;
}

bool EqualsExpression::operator==(LogicalExpression const& rhs) {
    const EqualsExpression* eq = dynamic_cast<const EqualsExpression*>(&rhs);
    if (!eq) return false;
    if (exprs.size() != eq->exprs.size()) return false;

    for (LogicalExpression* expr : eq->exprs) {
        // Find element of rhs in lhs
        auto find = std::find_if(exprs.begin(), exprs.end(), 
                [expr] (LogicalExpression* e) -> bool { return (*expr == *e);});
        if (find == exprs.end()) return false;
    }
    return true;
}

bool GreaterExpression::operator==(LogicalExpression const& rhs) {
    const GreaterExpression* gr = dynamic_cast<const GreaterExpression*>(&rhs);
    if (!gr) return false;
    if (exprs.size() != gr->exprs.size()) return false;

    for (size_t i = 0; i < exprs.size(); ++i) {
        if (*exprs[i] != *gr->exprs[i]) return false;
    }
    return true;
}

bool LowerExpression::operator==(LogicalExpression const& rhs) {
    const LowerExpression* lr = dynamic_cast<const LowerExpression*>(&rhs);
    if (!lr) return false;
    if (exprs.size() != lr->exprs.size()) return false;

    for (size_t i = 0; i < exprs.size(); ++i) {
        if (*exprs[i] != *lr->exprs[i]) return false;
    }
    return true;
}

bool GreaterEqualsExpression::operator==(LogicalExpression const& rhs) {
    const GreaterEqualsExpression* gr = 
        dynamic_cast<const GreaterEqualsExpression*>(&rhs);
    if (!gr) return false;
    if (exprs.size() != gr->exprs.size()) return false;

    for (size_t i = 0; i < exprs.size(); ++i) {
        if (*exprs[i] != *gr->exprs[i]) return false;
    }
    return true;
}

bool LowerEqualsExpression::operator==(LogicalExpression const& rhs) {
    const LowerEqualsExpression* lr = 
        dynamic_cast<const LowerEqualsExpression*>(&rhs);
    if (!lr) return false;
    if (exprs.size() != lr->exprs.size()) return false;

    for (size_t i = 0; i < exprs.size(); ++i) {
        if (*exprs[i] != *lr->exprs[i]) return false;
    }
    return true;
}

bool Addition::operator==(LogicalExpression const& rhs) {
    const Addition* add = dynamic_cast<const Addition*>(&rhs);
    if (!add) return false;
    if (exprs.size() != add->exprs.size()) return false;

    for (LogicalExpression* expr : add->exprs) {
        // Find element of rhs in lhs
        auto find = std::find_if(exprs.begin(), exprs.end(), 
                [expr] (LogicalExpression* e) -> bool { return (*expr == *e);});
        if (find == exprs.end()) return false;
    }
    return true;
}

// Just to be safe we don't allow commutativity for any of the elements
bool Subtraction::operator==(LogicalExpression const& rhs) {
    const Subtraction* sub = 
        dynamic_cast<const Subtraction*>(&rhs);
    if (!sub) return false;
    if (exprs.size() != sub->exprs.size()) return false;

    for (size_t i = 0; i < exprs.size(); ++i) {
        if (*exprs[i] != *sub->exprs[i]) return false;
    }
    return true;
}

bool Multiplication::operator==(LogicalExpression const& rhs) {
    const Multiplication* mult = dynamic_cast<const Multiplication*>(&rhs);
    if (!mult) return false;
    if (exprs.size() != mult->exprs.size()) return false;

    for (LogicalExpression* expr : mult->exprs) {
        // Find element of rhs in lhs
        auto find = std::find_if(exprs.begin(), exprs.end(), 
                [expr] (LogicalExpression* e) -> bool { return (*expr == *e);});
        if (find == exprs.end()) return false;
    }
    return true;
}

// Just to be safe we don't allow commutativity for any of the elements
bool Division::operator==(LogicalExpression const& rhs) {
    const Division* div = 
        dynamic_cast<const Division*>(&rhs);
    if (!div) return false;
    if (exprs.size() != div->exprs.size()) return false;

    for (size_t i = 0; i < exprs.size(); ++i) {
        if (*exprs[i] != *div->exprs[i]) return false;
    }
    return true;
}

/*****************************************************************
  Unaries
 *****************************************************************/

bool Negation::operator==(LogicalExpression const& rhs) {
    const Negation* neg = dynamic_cast<const Negation*>(&rhs);
    if (!neg) return false;
    return (*(neg->expr) == *expr);
}

bool KronDeltaDistribution::operator==(LogicalExpression const& rhs) {
    const KronDeltaDistribution* kron = 
        dynamic_cast<const KronDeltaDistribution*>(&rhs);
    if (!kron) return false;
    return (*(kron->expr) == *expr);
}

bool BernoulliDistribution::operator==(LogicalExpression const& rhs) {
    const BernoulliDistribution* bern = 
        dynamic_cast<const BernoulliDistribution*>(&rhs);
    if (!bern) return false;
    return (*(bern->expr) == *expr);
}

/*****************************************************************
  Conditionals
 *****************************************************************/

bool IfThenElseExpression::operator==(LogicalExpression const& rhs) {
    const IfThenElseExpression* iff = 
        dynamic_cast<const IfThenElseExpression*>(&rhs);
    if (!iff) return false;
    if (*condition != *(iff->condition)) return false;
    if (*valueIfTrue != *(iff->valueIfTrue)) return false;
    if (*valueIfFalse != *(iff->valueIfFalse)) return false;
    return true;
}

bool MultiConditionChecker::operator==(LogicalExpression const& rhs) {
    const MultiConditionChecker* mcc = 
        dynamic_cast<const MultiConditionChecker*>(&rhs);
    if (!mcc) return false;
    if (conditions.size() != mcc->conditions.size()) return false;
    if (effects.size() != mcc->effects.size()) return false;
    for (size_t i = 0; i < conditions.size(); ++i) {
        if (*conditions[i] != *(mcc->conditions[i])) return false;
    }
    for (size_t i = 0; i < effects.size(); ++i) {
        if (*effects[i] != *(mcc->effects[i])) return false;
    }
    return true;
}

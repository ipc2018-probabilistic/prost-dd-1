LogicalExpression* LogicalExpression::make_add_compatible() {
    assert(false);
    return this;
}

LogicalExpression* ParametrizedVariable::make_add_compatible() {
    return this;
}

LogicalExpression* NumericConstant::make_add_compatible() {
    return this;
}

LogicalExpression* Conjunction::make_add_compatible() {
    vector<LogicalExpression*> compatible_parts;
    for (LogicalExpression* expr : exprs) {
        compatible_parts.push_back(expr->make_add_compatible());
    }
    return new Conjunction(compatible_parts);
}

LogicalExpression* Disjunction::make_add_compatible() {
    vector<LogicalExpression*> compatible_parts;
    for (LogicalExpression* expr : exprs) {
        compatible_parts.push_back(expr->make_add_compatible());
    }
    return new Disjunction(compatible_parts);
}

LogicalExpression* EqualsExpression::make_add_compatible() {
    vector<LogicalExpression*> compatible_parts;
    for (LogicalExpression* expr : exprs) {
        compatible_parts.push_back(expr->make_add_compatible());
    }
    return new EqualsExpression(compatible_parts);
}

LogicalExpression* GreaterExpression::make_add_compatible() {
    vector<LogicalExpression*> compatible_parts;
    for (LogicalExpression* expr : exprs) {
        compatible_parts.push_back(expr->make_add_compatible());
    }
    return new GreaterExpression(compatible_parts);
}
LogicalExpression* GreaterEqualsExpression::make_add_compatible() {
    vector<LogicalExpression*> compatible_parts;
    for (LogicalExpression* expr : exprs) {
        compatible_parts.push_back(expr->make_add_compatible());
    }
    return new GreaterEqualsExpression(compatible_parts);
}

LogicalExpression* LowerExpression::make_add_compatible() {
    vector<LogicalExpression*> compatible_parts;
    for (LogicalExpression* expr : exprs) {
        compatible_parts.push_back(expr->make_add_compatible());
    }
    return new LowerExpression(compatible_parts);
}
LogicalExpression* LowerEqualsExpression::make_add_compatible() {
    vector<LogicalExpression*> compatible_parts;
    for (LogicalExpression* expr : exprs) {
        compatible_parts.push_back(expr->make_add_compatible());
    }
    return new LowerEqualsExpression(compatible_parts);
}
LogicalExpression* Addition::make_add_compatible() {
    vector<LogicalExpression*> compatible_parts;
    for (LogicalExpression* expr : exprs) {
        compatible_parts.push_back(expr->make_add_compatible());
    }
    return new Addition(compatible_parts);
}
LogicalExpression* Subtraction::make_add_compatible() {
    vector<LogicalExpression*> compatible_parts;
    for (LogicalExpression* expr : exprs) {
        compatible_parts.push_back(expr->make_add_compatible());
    }
    return new Subtraction(compatible_parts);
}

LogicalExpression* Multiplication::make_add_compatible() {
    vector<LogicalExpression*> compatible_parts;
    for (LogicalExpression* expr : exprs) {
        compatible_parts.push_back(expr->make_add_compatible());
    }
    return new Multiplication(compatible_parts);
}

LogicalExpression* Division::make_add_compatible() {
    vector<LogicalExpression*> compatible_parts;
    for (LogicalExpression* expr : exprs) {
        compatible_parts.push_back(expr->make_add_compatible());
    }
    return new Division(compatible_parts);
}

LogicalExpression* Negation::make_add_compatible() {
    LogicalExpression* comp = expr->make_add_compatible();
    return new Negation(comp);
}

LogicalExpression* ExponentialFunction::make_add_compatible() {
    return new ExponentialFunction(expr->make_add_compatible());
}

LogicalExpression* KronDeltaDistribution::make_add_compatible() {
    return expr->make_add_compatible();
}

LogicalExpression* BernoulliDistribution::make_add_compatible() {
    return expr->make_add_compatible();
}

LogicalExpression* IfThenElseExpression::make_add_compatible() {
    LogicalExpression* comp_condition = condition->make_add_compatible();
    Negation* negated_condition = new Negation(comp_condition);
    LogicalExpression* comp_value_if_true = valueIfTrue->make_add_compatible();
    LogicalExpression* comp_value_if_false =
        valueIfFalse->make_add_compatible();
    // reform to: condition * value_if_true + !condition * value_if_false
    vector<LogicalExpression*> true_parts = {comp_condition,
                                             comp_value_if_true};
    Multiplication* true_mult = new Multiplication(true_parts);
    vector<LogicalExpression*> false_parts = {negated_condition,
                                              comp_value_if_false};
    Multiplication* false_mult = new Multiplication(false_parts);
    Simplifications dummy;
    vector<LogicalExpression*> addition_parts = {true_mult->simplify(dummy),
                                                 false_mult->simplify(dummy)};
    return new Addition(addition_parts);
}

LogicalExpression* MultiConditionChecker::make_add_compatible() {
    vector<LogicalExpression*> current_conditions;
    vector<LogicalExpression*> condition_effect_mult;
    Simplifications dummy;
    for (size_t i = 0; i < conditions.size(); ++i) {
        auto comp_condition = conditions[i]->make_add_compatible();
        current_conditions.push_back(comp_condition);
        auto conjunction = new Conjunction(current_conditions);
        vector<LogicalExpression*> mult_parts{
            conjunction->simplify(dummy), effects[i]->make_add_compatible()};
        Multiplication* mult = new Multiplication(mult_parts);
        condition_effect_mult.push_back(mult->simplify(dummy));
        current_conditions.pop_back();
        // Subsequent effect only happens if condition did not hold
        current_conditions.push_back(new Negation(comp_condition));
    }
    return new Addition(condition_effect_mult);
}

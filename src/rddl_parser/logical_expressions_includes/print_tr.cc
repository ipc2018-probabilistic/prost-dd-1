void LogicalExpression::print_tr(ostream& /*out*/) const {
    std::cerr
        << "Error: requested transition relation for unsupported formula: ";
    print(cerr);
    cerr << endl;
    assert(false);
}

/*****************************************************************
                         Schematics
*****************************************************************/

void Parameter::print_tr(ostream& out) const {
    out << name << " ";
}

void StateFluent::print_tr(ostream& out) const {
    out << "s" << index << "";
}

void ParametrizedVariable::print_tr(ostream& out) const {
    out << fullName;
}

/*****************************************************************
                           Atomics
*****************************************************************/

void NumericConstant::print_tr(ostream& out) const {
    out << value;
}

/*****************************************************************
                           Connectives
*****************************************************************/

void Connective::print_tr_logic(ostream& out, string const& symbol) const {
    print_tr(out, "[", symbol, "]");
}

void Connective::print_tr_arithmetic(ostream& out, string const& symbol) const {
    print_tr(out, "(", symbol, ")");
}

void Connective::print_tr(ostream& out, string const& open_p,
                          string const& symbol, string const& close_p) const {
    out << open_p;
    for (unsigned int i = 0; i < exprs.size(); ++i) {
        exprs[i]->print_tr(out);
        if (i != exprs.size() - 1) {
            out << " " << symbol << " ";
        }
    }
    out << close_p;
}

void Conjunction::print_tr(ostream& out) const {
    Connective::print_tr_logic(out, "&&");
}

void Disjunction::print_tr(ostream& out) const {
    Connective::print_tr_logic(out, "||");
}

void EqualsExpression::print_tr(ostream& out) const {
    Connective::print_tr_logic(out, "==");
}

void GreaterExpression::print_tr(ostream& out) const {
    assert(exprs.size() == 2);
    Connective::print_tr_arithmetic(out, ">");
}

void LowerExpression::print_tr(ostream& out) const {
    assert(exprs.size() == 2);
    // Transform lhs <= rhs to -1*lhs >= -1*rhs
    vector<LogicalExpression*> lhs_parts{new NumericConstant(-1), exprs[0]};
    auto lhs = new Multiplication(lhs_parts);
    vector<LogicalExpression*> rhs_parts{new NumericConstant(-1), exprs[1]};
    auto rhs = new Multiplication(rhs_parts);
    vector<LogicalExpression*> greater_parts{lhs, rhs};
    auto greater = new GreaterExpression(greater_parts);
    greater->print_tr(out);
}

void GreaterEqualsExpression::print_tr(ostream& out) const {
    assert(exprs.size() == 2);
    Connective::print_tr_arithmetic(out, ">=");
}

void LowerEqualsExpression::print_tr(ostream& out) const {
    assert(exprs.size() == 2);
    // Transform lhs <= rhs to -1*lhs >= -1*rhs
    vector<LogicalExpression*> lhs_parts = {new NumericConstant(-1), exprs[0]};
    auto lhs = new Multiplication(lhs_parts);
    vector<LogicalExpression*> rhs_parts = {new NumericConstant(-1), exprs[1]};
    auto rhs = new Multiplication(rhs_parts);
    vector<LogicalExpression*> greater_parts{lhs, rhs};
    auto geq = new GreaterEqualsExpression(greater_parts);
    geq->print_tr(out);
}

void Addition::print_tr(ostream& out) const {
    Connective::print_tr_arithmetic(out, "+");
}

void Subtraction::print_tr(ostream& out) const {
    Connective::print_tr_arithmetic(out, "-");
}

void Multiplication::print_tr(ostream& out) const {
    Connective::print_tr_arithmetic(out, "*");
}

void Division::print_tr(ostream& out) const {
    Connective::print_tr_arithmetic(out, "/");
}

void ExponentialFunction::print_tr(ostream& out) const {
    // e^x = 2.71828 ^ x
    out << "(2.71828 ^ ";
    expr->print_tr(out);
    out << ")";
}


/*****************************************************************
                          Unaries
*****************************************************************/

void Negation::print_tr(ostream& out) const {
    out << "[!";
    expr->print_tr(out);
    out << "]";
}

void IfThenElseExpression::print_tr(ostream& out) const {
    // if f, then g, else h: f*g + ~f * h
    out << "(";
    condition->print(out);
    out << " * ";
    valueIfTrue->print(out);
    out << " + [!";
    condition->print(out);
    out << "] * ";
    valueIfFalse->print(out);
    out << ")";
}

//void MultiConditionChecker::print(ostream& out) const {
//    // switch (a -> a',
//    //         b -> b',
//    //         else -> c')
//    // = a*a' + ~a*b*b' + ~
//    out << "switch( ";
//    for (unsigned int i = 0; i < conditions.size(); ++i) {
//        out << "(";
//        conditions[i]->print(out);
//        out << " : ";
//        effects[i]->print(out);
//        out << ") ";
//    }
//    out << ")";
//}

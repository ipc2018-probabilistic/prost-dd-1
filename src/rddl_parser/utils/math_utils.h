#ifndef MATH_UTILS_H
#define MATH_UTILS_H

#define EPSILON 0.000000001 // std::numeric_limits<double>::epsilon()

#include <cmath>
#include <cstdlib>
#include <limits>
#include <vector>

class MathUtils {
public:
    static bool doubleIsEqual(double const& d1, double const& d2) {
        return std::fabs(d1 - d2) < EPSILON;
    }

    static bool doubleIsSmaller(double const& d1, double const& d2) {
        return d1 + EPSILON < d2;
    }

    static bool doubleIsGreater(double const& d1, double const& d2) {
        return d1 > d2 + EPSILON;
    }

    static bool doubleIsSmallerOrEqual(double const& d1, double const& d2) {
        return !doubleIsGreater(d1, d2);
    }

    static bool doubleIsGreaterOrEqual(double const& d1, double const& d2) {
        return !doubleIsSmaller(d1, d2);
    }

    static bool doubleIsMinusInfinity(double const& d1) {
        return doubleIsEqual(d1, -std::numeric_limits<double>::max());
    }

    static bool multiplyWithOverflowCheck(long& x, unsigned long const& y) {
        long base = x;
        x = 0;
        for (unsigned long i = 0; i < y; ++i) {
            x += base;
            if (x < base) {
                return false;
            }
        }
        return true;
    }

    static bool toThePowerOfWithOverflowCheck(long& x, unsigned int const& y) {
        long base = x;
        for (unsigned int i = 1; i < y; ++i) {
            if (!multiplyWithOverflowCheck(x, base)) {
                return false;
            }
        }
        return true;
    }

    // TODO: Make sure that this always generates a number that is smaller than
    // 1.0 - EPSILON
    static double generateRandomNumber() {
        return (double)(rand() % 1000001) / 1000001.0;
    }

    // Given <V1,...,V_n>, computes the cartesian product V1x...xV_n.
    // E.g. {1,2,3}x{5,6} = {{1,5},{1,6},{2,5},{2,6},{3,5},{3,6}}
    template <typename T>
    static std::vector<std::vector<T>> cart_product(
        std::vector<std::vector<T>> const& v) {
        std::vector<std::vector<T>> s = {{}};
        for (const auto& u : v) {
            std::vector<std::vector<T>> r;
            for (const auto& x : s) {
                for (const auto y : u) {
                    r.push_back(x);
                    r.back().push_back(y);
                }
            }
            s = move(r);
        }
        return s;
    }

private:
    MathUtils() {}
};

#endif

#include "json_task.h"

#include "../evaluatables.h"
#include "../rddl.h"
#include "../states.h"
#include "transition_relation.h"

#include <sstream>
#include <string>
#include <vector>

using std::string;

nlohmann::json JsonTask::to_json() const {
    nlohmann::json task_desc;
    add_variables(task_desc);
    add_initial_state(task_desc);
    add_goal(task_desc);
    add_actions(task_desc);
    return task_desc;
}

void JsonTask::add_variables(nlohmann::json& task_desc) const {
    for (ConditionalProbabilityFunction const* cpf : task->CPFs) {
        string name = "s" + std::to_string(cpf->head->index);
        task_desc["variables"][name]["domain"] = cpf->getDomainSize();
    }
    // Add fake goal
    task_desc["variables"]["fake_goal"]["domain"] = 2;
}

void JsonTask::add_initial_state(nlohmann::json& task_desc) const {
    for (ConditionalProbabilityFunction const* cpf : task->CPFs) {
        // TODO(FG) what if we have non-integer variable values?
        string name = "s" + std::to_string(cpf->head->index);
        task_desc["initial_state"][name] =
            static_cast<int>(cpf->getInitialValue());
    }
    task_desc["initial_state"]["fake_goal"] = 0;
}

void JsonTask::add_goal(nlohmann::json& task_desc) const {
    task_desc["goal_state"]["fake_goal"] = 1;
}

void JsonTask::add_actions(nlohmann::json& task_desc) const {
    if (task->actionStates.size() > 500) {
        // Corrupt json string to kill heuristic
        task_desc["actions"]["dummy"]["Tr"] = "-(";
        return;
    }
    for (ActionState const& action : task->actionStates) {
        TransitionRelation tr(task, action);
        std::stringstream ss;
        tr.print(ss);
        // Add transition relation to action description
        task_desc["actions"][action.getName()]["Tr"] = ss.str();
        task_desc["actions"][action.getName()]["Tc"] = cost_string(action);
    }
}

std::string JsonTask::cost_string(ActionState const& action) const {
    Simplifications replacements = action.asSimplifications(task);
    // Fix action in reward
    LogicalExpression* reward =
        task->rewardCPF->formula->simplify(replacements);
    // Transform unsupported expressions
    LogicalExpression* transformed_reward = reward->make_add_compatible();
    // Costs are negative reward.
    std::vector<LogicalExpression*> subtractionParts{new NumericConstant(0.0),
                                                     transformed_reward};
    LogicalExpression* cost = new Subtraction(subtractionParts);
    std::stringstream ss;
    cost->print_tr(ss);
    delete cost;
    delete reward;
    return ss.str();
}

#include "transition_relation.h"

#include "../evaluatables.h"
#include "../logical_expressions.h"
#include "../rddl.h"
#include "conditional_effect.h"

#include <limits>
#include <sstream>

using std::endl;
using std::vector;
using std::numeric_limits;
using std::string;
using std::stringstream;

TransitionRelationEffect::TransitionRelationEffect(StateFluent* head,
                                                   LogicalExpression* formula) {
    if (auto mcc = dynamic_cast<MultiConditionChecker*>(formula)) {
        conditional_effects = getCondEffs(mcc);
    } else if (auto iff = dynamic_cast<IfThenElseExpression*>(formula)) {
        conditional_effects = getCondEffs(iff);
    } else {
        conditional_effects = {
            ConditionalEffect(new NumericConstant(1.0), formula)};
    }
    effect_var = head;
}

vector<ConditionalEffect> TransitionRelationEffect::getCondEffs(
    MultiConditionChecker* mcc) const {
    vector<ConditionalEffect> res;
    vector<LogicalExpression*> conditions;
    Simplifications dummy;
    for (size_t i = 0; i < mcc->conditions.size(); ++i) {
        conditions.push_back(mcc->conditions[i]);
        auto conjunction = new Conjunction(conditions);
        res.emplace_back(conjunction->simplify(dummy), mcc->effects[i]);
        conditions.pop_back();
        // Subsequent effect only happens if condition did not hold
        conditions.push_back(new Negation(mcc->conditions[i]));
    }
    return res;
}

vector<ConditionalEffect> TransitionRelationEffect::getCondEffs(
    IfThenElseExpression* iff) const {
    vector<ConditionalEffect> res;
    Simplifications dummy;
    res.emplace_back(iff->condition, iff->valueIfTrue);
    auto negation = new Negation(iff->condition);
    res.emplace_back(negation->simplify(dummy), iff->valueIfFalse);
    return res;
}

void TransitionRelationEffect::makeEffectsDiscrete(RDDLTask const* task) {
    for (ConditionalEffect& conditional_effect : conditional_effects) {
        conditional_effect.effect =
            make_discrete(conditional_effect.effect, task);
    }
}

LogicalExpression* TransitionRelationEffect::make_discrete(
    LogicalExpression* expr, RDDLTask const* task) const {
    Simplifications dummy;
    // If expr is already discrete, only determinize
    auto discrete = dynamic_cast<DiscreteDistribution*>(expr);
    if (discrete) {
        // Transform switch cases in discrete effects to arithmetic expression
        for (size_t i = 0; i < discrete->probabilities.size(); ++i) {
            LogicalExpression* expr = discrete->probabilities[i];
            if (auto mcc = dynamic_cast<MultiConditionChecker*>(expr)) {
                auto conditional_effects = getCondEffs(mcc);
                vector<LogicalExpression*> sum_parts;
                for (auto cond_eff : conditional_effects) {
                    vector<LogicalExpression*> mult{cond_eff.condition,
                                                    cond_eff.effect};
                    sum_parts.push_back(new Multiplication(mult));
                }
                auto add = new Addition(sum_parts);
                discrete->probabilities[i] = add;
            }
        }
        return determinize(discrete, task);
    }
    // Turn bernoulli distribution into discrete distribution
    auto bern = dynamic_cast<BernoulliDistribution*>(expr);
    if (bern) {
        LogicalExpression* true_probability = bern->make_add_compatible();
        vector<LogicalExpression*> parts = {NumericConstant::one,
                                            true_probability};
        LogicalExpression* false_probability = new Subtraction(parts);
        auto threshold = new NumericConstant(task->getPruningParameter());
        vector<LogicalExpression*> greater_parts{true_probability, threshold};
        auto greater_true = new GreaterEqualsExpression(greater_parts);
        greater_parts[0] = false_probability;
        auto greater_false = new GreaterEqualsExpression(greater_parts);
        // Discrete distribution with 0.0 <-> 1 - probability, 1.0 <->
        // probability
        return new DiscreteDistribution(
            {NumericConstant::zero, NumericConstant::one},
            {greater_false->simplify(dummy), greater_true->simplify(dummy)});
    }
    // Otherwise this is a KronDelta Distribution and we do not have to
    // transform it
    return expr;
}

// This is basically a copy of DiscreteDistribution::determinize, but instead of
// a MCC it returns a discrete distribution, since we use these as transition
// formulae. This way we do not interfer with determinization of discrete
// distributions used in other parts in Prost (which may assume that a
// determinized formula is never a discrete distribution).
DiscreteDistribution* TransitionRelationEffect::determinize(
    DiscreteDistribution* dist, RDDLTask const* task) const {
    // Determinize each discrete probability only once:
    vector<LogicalExpression*> determinizedProbs;
    vector<double> upperBounds;
    Simplifications dummy;
    double maxLowerBound = -numeric_limits<double>::max();

    for (LogicalExpression* expr : dist->probabilities) {
        // Determinize all probabilities
        auto determinized = expr->determinizeMostLikely(task->actionStates);
        determinizedProbs.push_back(determinized->simplify(dummy));
        // Compute the bounds for all determinized probabilities
        double lower = numeric_limits<double>::max();
        double upper = -numeric_limits<double>::max();
        determinizedProbs.back()->determineBounds(task->actionStates, lower,
                                                  upper);
        maxLowerBound = std::max(maxLowerBound, lower);
        upperBounds.push_back(upper);
    }

    // Do not consider conditions which maxRes + 0.05 is smaller than another
    // conditions minRes (their conjunction can never evaluate to true)
    vector<LogicalExpression*> newValues;
    vector<LogicalExpression*> newProbabilities;
    for (size_t i = 0; i < dist->probabilities.size(); ++i) {
        if (upperBounds[i] + 0.05 >= maxLowerBound) {
            auto det_value =
                dist->values[i]->determinizeMostLikely(task->actionStates);
            newValues.push_back(det_value->simplify(dummy));
            newProbabilities.push_back(determinizedProbs[i]);
        }
    }

    // Determinization of a discrete distribution returns a distribution where
    // the probability of each value checks if the probability is higher than
    // ALL probabilities of other values minus a threshold (0.05).
    vector<LogicalExpression*> finalized_probabilities;

    for (size_t i = 0; i < newProbabilities.size(); ++i) {
        vector<LogicalExpression*> comparisons;
        for (size_t j = 0; j < newProbabilities.size(); ++j) {
            if (i == j) {
                continue;
            }
            // TODO We determinize such that all probabilities in the range of
            // 5% of the highest probability are accepted, i.e. p >= (p' - 0.05)
            vector<LogicalExpression*> subtraction_parts{
                newProbabilities[j], new NumericConstant(0.05)};
            auto threshold = new Subtraction(subtraction_parts);
            vector<LogicalExpression*> compareParts{newProbabilities[i],
                                                    threshold};
            auto geq = new GreaterEqualsExpression(compareParts);
            comparisons.push_back(geq->simplify(dummy));
        }
        auto conjunction = new Conjunction(comparisons);
        finalized_probabilities.push_back(conjunction->simplify(dummy));
    }
    return new DiscreteDistribution(newValues, finalized_probabilities);
}

void TransitionRelation::print(std::ostream& out) const {
    Simplifications replacements = action.asSimplifications(task);
    // Surround with action precondition
    if (!action.relevantSACs.empty()) {
        out << "(";
        for (ActionPrecondition const* precondition : action.relevantSACs) {
            LogicalExpression* fixedPrecondition =
                precondition->formula->simplify(replacements);
            fixedPrecondition->print_tr(out);
            if (precondition != action.relevantSACs.back()) {
                out << " * ";
            }
        }
        out << ") * ";
    }

    out << "(";
    for (ConditionalProbabilityFunction* cpf : task->CPFs) {
        TransitionRelationEffect tr(cpf->head,
                                    cpf->formula->simplify(replacements));
        tr.makeEffectsDiscrete(task);
        out << "(";
        print(out, tr);
        out << ")";
        // Formulae of effects are multiplied.
        if (cpf != task->CPFs.back()) {
            out << " * ";
        }
    }
    out << ")";
}

void TransitionRelation::print(std::ostream& out,
                               TransitionRelationEffect const& tr) const {
    std::string primed_var =
        "s" + std::to_string(tr.effect_var->index) + "_primed";
    for (ConditionalEffect cond_eff : tr.conditional_effects) {
        print_transition(out, primed_var, cond_eff);
        if (cond_eff != tr.conditional_effects.back())
            out << " + ";
    }
}

void TransitionRelation::print_transition(
    std::ostream& out, std::string const& primed_var,
    ConditionalEffect const& cond_eff) const {
    // condition * (sum_i ([v' == effect[i].value] * effect[i].probability))
    cond_eff.condition->print_tr(out);
    auto constant = dynamic_cast<NumericConstant*>(cond_eff.condition);
    if (constant && constant->value == 0.0) {
        // In rare cases (e.g. crossing_traffic cpf s10) the condition may be
        // false, we filter these cases here.
        return;
    }
    // If this is not a discrete effect it is a KronDelta effect
    auto discrete = dynamic_cast<DiscreteDistribution*>(cond_eff.effect);
    if (!discrete) {
        // e.g. cond*([s' == s])
        out << " * ([" << primed_var << "==";
        cond_eff.effect->print_tr(out);
        out << "])";
        return;
    }
    out << " * (";
    stringstream effect_ss;
    for (size_t i = 0; i < discrete->values.size(); ++i) {
        constant = dynamic_cast<NumericConstant*>(discrete->probabilities[i]);
        // If probability is zero, just multiply with 0
        if (constant && constant->value == 0.0) {
            continue;
        }
        effect_ss << "[" << primed_var << "==";
        discrete->values[i]->print_tr(effect_ss);
        effect_ss << "] * (";
        discrete->probabilities[i]->print_tr(effect_ss);
        effect_ss << ") + ";
        // If probability is 1, no other effect may happen and we can stop here
        // if (constant && constant->value == 1.0) {
        //     out << ")";
        //     return;
        // }
    }
    string effect_string = effect_ss.str();
    // Remove trailing " + " which is possible if the last effect was 0
    if (!effect_string.empty() &&
        effect_string.substr(effect_string.size() - 3, string::npos) == " + ") {
        effect_string = effect_string.substr(0, effect_string.size() - 3);
    }
    out << effect_string;
    out << ")";
}


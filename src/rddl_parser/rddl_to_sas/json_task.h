#ifndef JSON_TASK_H
#define JSON_TASK_H

#include "../../search/nlohmann/json.h"

#include <string>

class RDDLTask;
class ActionState;

// Class to print necessary task information into json format
class JsonTask {
public:
    JsonTask(RDDLTask* task) : task(task) {}
    nlohmann::json to_json() const;

private:
    RDDLTask const* task;
    // Adds the necessary fields to the json object
    void add_variables(nlohmann::json& task_desc) const;
    void add_initial_state(nlohmann::json& task_desc) const;
    void add_goal(nlohmann::json& task_desc) const;
    void add_actions(nlohmann::json& task_desc) const;
    // Returns the cost string for the given action.
    std::string cost_string(ActionState const& action) const;
};

#endif /* JSON_TASK_H */

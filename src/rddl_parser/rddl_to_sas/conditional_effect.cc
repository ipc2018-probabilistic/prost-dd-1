#include "conditional_effect.h"

#include "../logical_expressions.h"

void ConditionalEffect::print(std::ostream& out) const {
    out << "<";
    condition->print(out);
    out << ",";
    effect->print(out);
    out << ">";
    out << std::endl;
}

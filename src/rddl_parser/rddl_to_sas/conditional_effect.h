#ifndef CONDITIONAL_EFFECT_H
#define CONDITIONAL_EFFECT_H

#include <iostream>

class LogicalExpression;

struct ConditionalEffect {
    ConditionalEffect(LogicalExpression* cond, LogicalExpression* eff)
        : condition(cond), effect(eff) {}
    void print(std::ostream& out) const;

    friend bool operator==(const ConditionalEffect& lhs,
                           const ConditionalEffect& rhs) {
        return ((lhs.condition == rhs.condition) && (lhs.effect == rhs.effect));
    }
    friend bool operator!=(const ConditionalEffect& lhs,
                           const ConditionalEffect& rhs) {
        return !(lhs == rhs);
    }

    LogicalExpression* condition;
    LogicalExpression* effect;
};

#endif /* CONDITIONAL_EFFECT_H */

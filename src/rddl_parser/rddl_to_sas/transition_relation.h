#ifndef TRANSITION_RELATION_H
#define TRANSITION_RELATION_H

#include <iostream>
#include <string>
#include <vector>

class RDDLTask;
class ConditionalEffect;
class StateFluent;
class ConditionalProbabilityFunction;
class LogicalExpression;
class IfThenElseExpression;
class MultiConditionChecker;
class DiscreteDistribution;
class ActionState;

// Encodes the effects of a conditional probability function
struct TransitionRelationEffect {
    TransitionRelationEffect(StateFluent* head, LogicalExpression* formula);

    StateFluent* effect_var;
    std::vector<ConditionalEffect> conditional_effects;

    // TODO(FG) decouple
    std::vector<ConditionalEffect> getCondEffs(IfThenElseExpression* iff) const;
    std::vector<ConditionalEffect> getCondEffs(MultiConditionChecker* mc) const;

    // Transforms the effect of each conditional effect into a discrete
    // distribution
    void makeEffectsDiscrete(RDDLTask const* task);
    LogicalExpression* make_discrete(LogicalExpression* expr,
                                     RDDLTask const* task) const;
    DiscreteDistribution* determinize(DiscreteDistribution* dist,
                                      RDDLTask const* task) const;
};

// Prints symbolic transition relations based on cpf formulae
class TransitionRelation {
public:
    TransitionRelation(RDDLTask const* task, ActionState const& action)
        : task(task), action(action) {}

    // Prints the transition relation formula for all CPFs.
    void print(std::ostream& out) const;

private:
    RDDLTask const* task;
    ActionState const& action;

    void print(std::ostream& out, TransitionRelationEffect const& tr) const;
    void print_transition(std::ostream& out, std::string const& primed_var,
                          ConditionalEffect const& cond_eff) const;
};

#endif /* TRANSITION_RELATION_H */

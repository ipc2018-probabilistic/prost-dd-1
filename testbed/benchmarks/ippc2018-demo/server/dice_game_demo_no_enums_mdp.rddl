////////////////////////////////////////////////////////////////////
//
// RDDL MDP version without enums of the Dice Game demo domain for 
// IPC 2018 by Thomas Keller (tho.keller [at] unibas.ch).
//
// This is a simple domain to show the usage of enum-valued
// state-fluents. In each step, one die may be rolled, and the reward is
// the sum of the pins on all (not just the rolled) die.
//
////////////////////////////////////////////////////////////////////

domain dice_game_demo_no_enums_mdp {
    requirements = { 
        reward-deterministic,
        preconditions
    };

    types {
        die : object;
    };

    pvariables {
        die-value-is-one-initially : {state-fluent, bool, default = true};

        // interm-fluents to replace the small-int enum
        die-value-is-one(die)   : {interm-fluent, bool, level = 1};
        die-value-is-two(die)   : {interm-fluent, bool, level = 2};
        die-value-is-three(die) : {interm-fluent, bool, level = 3};
        die-value-is-four(die)  : {interm-fluent, bool, level = 4};
        die-value-is-five(die)  : {interm-fluent, bool, level = 5};
        die-value-is-six(die)   : {interm-fluent, bool, level = 6};

        // action-fluents
        roll(die)       : {action-fluent, bool, default = false};
    };

    cpfs {
        // We need this one because the die-value of all dice is 1
        // initially in the enum version         
        die-value-is-one-initially = false;

        // Since exactly one of these has to become true, we have to
        // deal with them sequentially (by using interm-fluents of
        // increasing levels) and base each result on the results before        
        die-value-is-one'(?d) =
            if ( roll(?d) )
                then Bernoulli(1/6)
            else die-value-is-one(?d) | die-value-is-one-initially;

        die-value-is-two'(?d) =
            if ( roll(?d) )
                then ( ~die-value-is-one(?d) & Bernoulli(1/5) )
            else die-value-is-two(?d);
    
        die-value-is-three'(?d) =
            if ( roll(?d) )
                then ( ~die-value-is-one(?d) & ~die-value-is-two(?d) & Bernoulli(1/4) )
            else die-value-is-three(?d);

        die-value-is-four'(?d) =
            if ( roll(?d) )
                then ( ~die-value-is-one(?d) & ~die-value-is-two(?d) & ~die-value-is-three(?d) & Bernoulli(1/3) )
            else die-value-is-four(?d);

        die-value-is-five'(?d) =
            if ( roll(?d) )
                then ( ~die-value-is-one(?d) & ~die-value-is-two(?d) & ~die-value-is-three(?d) & ~die-value-is-four(?d) & Bernoulli(1/2) )
            else die-value-is-five(?d);

        die-value-is-six'(?d) =
            if ( roll(?d) )
                then ( ~die-value-is-one(?d) & ~die-value-is-two(?d) & ~die-value-is-three(?d) & ~die-value-is-four(?d) & ~die-value-is-five(?d) )
            else die-value-is-six(?d);
    };
    
    reward = sum_{?d : die} [ (1 * die-value-is-one(?d))   + (2 * die-value-is-two(?d)) +
                              (3 * die-value-is-three(?d)) + (4 * die-value-is-four(?d)) +
                              (5 * die-value-is-five(?d))  + (6 * die-value-is-six(?d)) ]; 
        
    action-preconditions {
        // only one die may be rolled at a time
        ( sum_{?d : die} [ roll(?d) ] ) <= 1;
    };
}




Bootstrap: docker
From: ubuntu:bionic

%setup
     ## The "%setup"-part of this script is called to bootstrap an empty
     ## container. It copies the source files from the branch of your
     ## repository where this file is located into the container to the
     ## directory "/planner". Do not change this part unless you know
     ## what you are doing and you are certain that you have to do so.

    REPO_ROOT=`dirname $SINGULARITY_BUILDDEF`
    cp -r $REPO_ROOT/ $SINGULARITY_ROOTFS/planner

%post

    ## The "%post"-part of this script is called after the container has
    ## been created with the "%setup"-part above and runs "inside the
    ## container". Most importantly, it is used to install dependencies
    ## and build the planner. Add all commands that have to be executed
    ## once before the planner runs in this part of the script.

    ## Install all necessary dependencies.
    apt-get update
    apt-get -y install mercurial g++ g++-multilib make cmake bison flex libbdd-dev python automake autoconf libtool

    ## Build your planner

    ## 1. Parser
    cd /planner/src/rddl_parser
    make -j 6

    ## 2. CUDD
    cd ../search/cudd-3.0.0
    touch *
    ./configure --enable-obj --enable-dddmp "CFLAGS=-m64 -Wall -Wextra -g -O3" "CXXFLAGS=-m64 -Wall -Wextra -g -std=c++14 -O3" "LDFLAGS=-m64"
    make -j 6

    ## 3. Search
    cd ..
    make -j 6

    cp ../rddl_parser/rddl-parser ./

%runscript
    ## The runscript is called whenever the container is used to solve
    ## an instance.

    INSTANCE=$1
    PORT=$2

    ## Call your planner
    pwd
    cd /planner/src/search
    ./prost $INSTANCE -h 127.0.0.1 -p $PORT -prob 0.5 [PROST -tm UNI -ram 3900000 -s 0 -se [IPPC2018 -rec [MPA]]]

## Update the following fields with meta data about your submission.
## Please use the same field names and use only one line for each value.
%labels
Name                  prost-dd-1
Description           PROST enhanced by symbolic search with most-likely determinization
Authors               Florian Geißer, David Speck <geisserf,speckd@tf.uni-freiburg.de>
SupportsIntermFluents no
SupportsEnums         yes
